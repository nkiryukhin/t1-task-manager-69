package ru.t1.nkiryukhin.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.nkiryukhin.tm.api.service.dto.IDTOService;
import ru.t1.nkiryukhin.tm.comparator.CreatedComparator;
import ru.t1.nkiryukhin.tm.comparator.NameComparator;
import ru.t1.nkiryukhin.tm.dto.model.AbstractModelDTO;
import ru.t1.nkiryukhin.tm.repository.dto.AbstractDTORepository;

import java.util.Comparator;

@Service
public abstract class AbstractDTOService<M extends AbstractModelDTO> implements IDTOService<M> {

    @NotNull
    protected abstract AbstractDTORepository<M> getRepository();

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        if (comparator == NameComparator.INSTANCE) return "name";
        else return "status";
    }

    @Nullable
    @Override
    @Transactional
    public M add(@Nullable final M model) {
        if (model == null) return null;
        getRepository().save(model);
        return model;
    }

    @Override
    @Transactional
    public void removeOne(@Nullable final M model) {
        if (model == null) return;
        getRepository().delete(model);
    }

    @Override
    @Transactional
    public void update(@Nullable final M model) {
        if (model == null) return;
        getRepository().save(model);
    }

}
