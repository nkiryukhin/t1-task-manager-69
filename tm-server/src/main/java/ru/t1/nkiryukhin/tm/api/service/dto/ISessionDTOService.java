package ru.t1.nkiryukhin.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.dto.model.SessionDTO;
import ru.t1.nkiryukhin.tm.exception.field.AbstractFieldException;
import ru.t1.nkiryukhin.tm.exception.field.UserIdEmptyException;

import java.util.List;

public interface ISessionDTOService extends IDTOService<SessionDTO> {

    void clear(@Nullable String userId) throws UserIdEmptyException;

    boolean existsById(@Nullable String id) throws AbstractFieldException;

    List<SessionDTO> findAll(@Nullable String userId) throws UserIdEmptyException;

    SessionDTO findOneById(@Nullable String id) throws AbstractFieldException;

    int getSize();

    void remove(List<SessionDTO> sessions);

    void removeById(@Nullable String id) throws AbstractFieldException;

}