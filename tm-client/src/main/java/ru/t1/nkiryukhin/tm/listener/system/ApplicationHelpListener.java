package ru.t1.nkiryukhin.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.nkiryukhin.tm.dto.event.ConsoleEvent;
import ru.t1.nkiryukhin.tm.listener.AbstractListener;

@Component
public final class ApplicationHelpListener extends AbstractSystemListener {

    @NotNull
    @Override
    public String getArgument() {
        return "-h";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show command list";
    }

    @NotNull
    @Override
    public String getName() {
        return "help";
    }

    @Override
    @EventListener(condition = "@applicationHelpListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[HELP]");
        for (final AbstractListener listener : listeners) System.out.println(listener);
    }

}
