package ru.t1.nkiryukhin.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.nkiryukhin.tm.model.CustomUser;
import ru.t1.nkiryukhin.tm.repository.ProjectRepository;

@Controller
public class ProjectsController {

    @Autowired
    private ProjectRepository projectRepository;

    @GetMapping("/projects")
    public ModelAndView index(@AuthenticationPrincipal final CustomUser user) {
        return new ModelAndView("project-list", "projects", projectRepository.findAllByUserId(user.getUserId()));
    }

}