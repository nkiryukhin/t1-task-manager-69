package ru.t1.nkiryukhin.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.t1.nkiryukhin.tm.dto.UserDTO;
import ru.t1.nkiryukhin.tm.model.User;

public interface UserDTORepository extends JpaRepository<UserDTO, String> {

    UserDTO findByLogin(String login);

}