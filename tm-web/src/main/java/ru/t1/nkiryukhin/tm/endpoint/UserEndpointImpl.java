package ru.t1.nkiryukhin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.nkiryukhin.tm.api.endpoint.UserEndpoint;
import ru.t1.nkiryukhin.tm.dto.UserDTO;
import ru.t1.nkiryukhin.tm.model.User;
import ru.t1.nkiryukhin.tm.repository.UserDTORepository;
import ru.t1.nkiryukhin.tm.repository.UserRepository;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/users")
@WebService(endpointInterface = "ru.t1.nkiryukhin.tm.api.endpoint.UserEndpoint")
public class UserEndpointImpl implements UserEndpoint {

    @Autowired
    private UserDTORepository userDTORepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void clear() throws Exception {
        userRepository.deleteAll();
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() throws Exception {
        return userDTORepository.count();
    }

    @Override
    @WebMethod
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @NotNull final String id
    ) throws Exception {
        userRepository.deleteById(id);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @WebParam(name = "user", partName = "user")
            @RequestBody @NotNull final User user
    ) throws Exception {
        userRepository.deleteById(user.getId());
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @NotNull final String id
    ) throws Exception {
        return userDTORepository.findById(id).isPresent();
    }

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<UserDTO> findAll() throws Exception {
        return userDTORepository.findAll();
    }

    @Nullable
    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public UserDTO findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @NotNull final String id
    ) throws Exception {
        return userDTORepository.findById(id).orElse(null);
    }

    @NotNull
    @Override
    @WebMethod
    @PostMapping("/save")
    public UserDTO save(
            @WebParam(name = "user", partName = "user")
            @RequestBody @NotNull final UserDTO user
    ) throws Exception {
        return userDTORepository.save(user);
    }

}