package ru.t1.nkiryukhin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.nkiryukhin.tm.dto.ProjectDTO;
import ru.t1.nkiryukhin.tm.dto.TaskDTO;

import java.util.Collection;
import java.util.Optional;


@Repository
public interface TaskDTORepository extends JpaRepository<TaskDTO, String> {

    long countByUserId(@NotNull final String userId);

    void deleteAllByUserId(@NotNull final String userId);

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    boolean existsByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @NotNull
    Collection<TaskDTO> findAllByUserId(@NotNull final String userId);

    @NotNull
    Optional<TaskDTO> findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

}