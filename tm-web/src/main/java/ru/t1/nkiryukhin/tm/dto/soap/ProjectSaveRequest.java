package ru.t1.nkiryukhin.tm.dto.soap;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.nkiryukhin.tm.model.Project;

import javax.xml.bind.annotation.*;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "projectSaveRequest")
public class ProjectSaveRequest {

    @XmlElement
    private Project project;

}