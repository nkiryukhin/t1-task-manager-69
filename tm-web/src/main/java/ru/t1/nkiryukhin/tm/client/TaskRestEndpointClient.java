package ru.t1.nkiryukhin.tm.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.t1.nkiryukhin.tm.model.Task;

import java.util.Collection;
import java.util.List;

public interface TaskRestEndpointClient {

    String BASE_URL = "http://localhost:8080/api/tasks";

    static TaskRestEndpointClient client() {
        @NotNull final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(TaskRestEndpointClient.class, BASE_URL);
    }

    @GetMapping("/findAll")
    Collection<Task> findAll();

    @PostMapping("/save")
    void save(@RequestBody Task task);

    @GetMapping("/findById/{id}")
    Task findById(@PathVariable("id") String id);

    @GetMapping("/existsById/{id}")
    boolean existsById(@PathVariable("id") String id);

    @GetMapping("/count")
    long count();

    @PostMapping("/deleteById/{id}")
    void deleteById(@PathVariable("id") String id);

    @PostMapping("/delete")
    void delete(@RequestBody Task task);

    @PostMapping("/deleteAll")
    void clear(@RequestBody List<Task> tasks);

    @PostMapping("/clear")
    void clear();

}