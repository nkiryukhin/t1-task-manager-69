package ru.t1.nkiryukhin.tm.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.t1.nkiryukhin.tm.model.CustomUser;

import java.nio.file.AccessDeniedException;

public final class UserUtil {

    private UserUtil() {
    }

    public static String getUserId() throws AccessDeniedException {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final Object principal = authentication.getPrincipal();
        if (principal == null) throw new AccessDeniedException("");
        if (!(principal instanceof CustomUser)) throw new AccessDeniedException("");
        final CustomUser customUser = (CustomUser) principal;
        return customUser.getUserId();
    }

}